#include <thk_pid_controller.h>

double prop_gain = 0.1;
double int_gain = 0.1;
double diff_gain = 0.01;

double input_value = 0;
uint8_t step = 0;
uint8_t setpoint = 50;

thk_PIDController pid(prop_gain, int_gain, diff_gain, 0);

void setup(){
    Serial.begin(115200);
    pid.set_setpoint(setpoint);
}

void loop(){
    step++;

    input_value = pid.compute(input_value);

    if(setpoint - input_value < 0.01)
    {
        Serial.print("Reached setpoint after ");
        Serial.print(step);
        Serial.print(" steps");
        exit(0);
    }
    else
    {
        Serial.print(step);
        Serial.print(": ");
        Serial.print(input_value);
        Serial.print("\n");
    }

    delay(100);

}