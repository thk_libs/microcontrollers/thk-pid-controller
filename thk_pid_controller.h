#ifndef PID_CONTROLLER_H
#define PID_CONTROLLER_H

#include "Arduino.h"

class thk_PIDController
{
    public:
        thk_PIDController(double Kp, double Ki, double Kd, double setpoint);
        double compute(double input_value);
        void set_setpoint(double setpoint);
        void reset();

    private:
        double Kp, Ki, Kd;
        double setpoint;
        double out_p, out_i, out_d;

};

#endif