# PID Controller

Mit dieser Bibliothek kann ein PID Regler erzeugt werden, der mit Hilfe einer Führungsgröße und dem Ist-Wert die Regelgröße berechnet.

## **Installation:**

Um diese Klassen verwenden zu können, muss dieses Repository geklont und in das Library-Verzeichnis der Arduino-IDE kopiert werden.<br />
<br />

## **Anwendung:**

Zur Verwendung siehe zunächst das Beispiel `pid_example.ino`<br />
<br />

**Einbinden der Bibliothek:**
```Arduino
#include <thk_pid_controller.h>
```

**Instanziieren:**
```Arduino
thk_PIDController pid_controller(proportional_gain, integrational_gain, differential_gain, setpoint);
```

- `proportional_gain`: Proportional Faktor
- `integrational_gain`: Integral Faktor
- `differential_gain`: Differential Faktor
- `setpoint`: Führungsgröße<br />
<br />

### **Funktionen:**

**Übergeben einer neuen Führungsgröße an den Regler:**
```Arduino
pid_controller.set_setpoint(setpoint);
``` 

**Berechnen der Regelgröße:**
```Arduino
controlled_variable = pid_controller.compute(input_value);
``` 

**Zurücksetzen von I- und D-Anteil:**
```Arduino
pid_controller.reset();
``` 



